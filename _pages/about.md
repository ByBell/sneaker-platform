---
layout: page
title: À propos
permalink: /about
comments: false
image: assets/images/screenshot.jpg
imageshadow: true
---

This website is a demonstration to see **Memoirs Jekyll theme** in action. The theme is compatible with Github pages, in fact even this demo itself is created with Github Pages and hosted with Github. 

<a target="_blank" href="blank" class="btn btn-dark"> CTA &rarr;</a>

